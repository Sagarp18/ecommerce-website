package biz.dss.Ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@SpringBootApplication
@Controller
public class EcommerceApplication {

	public static void main(String[] args) throws Throwable {
		SpringApplication.run(EcommerceApplication.class, args);
	}

	@GetMapping("/login")
	public String login() {
		return "login";
	}

	@GetMapping("/secure/{page}")
	public String secure(@PathVariable String page) {
		return "secure/" + page;
	}
}
